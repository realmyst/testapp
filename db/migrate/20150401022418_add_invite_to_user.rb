class AddInviteToUser < ActiveRecord::Migration
  def change
    add_reference :users, :invite, index: true
    add_foreign_key :users, :invites
  end
end
