class CreateInviteAttempts < ActiveRecord::Migration
  def change
    create_table :invite_attempts do |t|
      t.references :invite, index: true

      t.timestamps null: false
    end
    add_foreign_key :invite_attempts, :invites
  end
end
