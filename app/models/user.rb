class User < ActiveRecord::Base
  has_secure_password validations: false

  belongs_to :invite

  validates :email, presence: true, uniqueness: true, email: true
  validates :confirmation_token, uniqueness: true, allow_nil: true

  state_machine :state, initial: :unconfirmed do
    state :unconfirmed
    state :active

    event :confirm do
      transition unconfirmed: :active
    end
  end

  include UserRepository

  def guest?
    false
  end
end
