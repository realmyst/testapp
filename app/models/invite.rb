class Invite < ActiveRecord::Base
  validates :email, presence: true, uniqueness: true, email: true
  validates :token, presence: true, uniqueness: true

  has_many :attempts, dependent: :destroy

  after_initialize :generate_token

  state_machine :state, initial: :not_accepted do
    state :accepted
    state :not_accepted

    event :accept do
      transition not_accepted: :accepted
    end
  end

  include InviteRepository

  def new_attempt
    if attempts.empty? || Time.now - attempts.last.created_at > configus.invites.period
      attempts.create
    else
      errors.add(:attempts, :too_often)
      false
    end
  end

  def to_param
    token
  end

  private

  def generate_token
    self.token ||= SecureHelper.generate_token
  end
end
