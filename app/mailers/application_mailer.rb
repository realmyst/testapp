class ApplicationMailer < ActionMailer::Base
  default from: configus.mailers.from
  layout nil
end
