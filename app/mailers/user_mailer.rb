class UserMailer < ApplicationMailer
  def confirm_email(user)
    @user = user
    mail(to: @user.email, subject: t('.subject'))
  end

  def success_confirm(user)
    @user = user
    mail(to: @user.email, subject: t('.subject'))
  end

  def send_invite(invite)
    @invite = invite
    mail(to: invite.email, subject: t('.subject'))
  end
end
