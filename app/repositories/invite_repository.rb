module InviteRepository
  extend ActiveSupport::Concern

  included do
    scope :not_accepted, -> { where(state: :not_accepted) }
  end
end
