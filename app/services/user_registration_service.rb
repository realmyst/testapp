class UserRegistrationService
  attr_reader :user

  def initialize(user, invite = nil)
    @user = user
    @invite = invite
  end

  def register
    user.confirmation_token = SecureHelper.generate_token
    if @invite
      @invite.accept
      user.invite = @invite
    end
    user.save
    UserMailer.confirm_email(user).deliver_later
  end

  def confirm
    if user && user.confirm
      UserMailer.success_confirm(user).deliver_later
    else
      false
    end
  end
end
