class InvitationsService
  attr_reader :errors

  def initialize(email = nil)
    @email = email
  end

  def invite_user
    invite ||= Invite.find_or_create_by(email: @email)
    if invite.new_attempt
      UserMailer.send_invite(invite).deliver_later
      true
    else
      @errors = invite.errors
      false
    end
  end
end
