module Concerns
  module FlashHelper
    def f(key, options = {} )
      scope = [:flash]
      scope << params[:controller].split('/')
      scope << params[:action]

      msg = options[:messages] || I18n.t(key, scope: scope)
      Rails.logger.debug("flash: #{msg}")
      if options[:now]
        flash.now[key] = msg
      else
        flash[key] = msg
      end
    end
  end
end
