class Web::InvitesController < Web::ApplicationController
  skip_before_filter :authenticate_user!, only: [:accept]

  def create
    @invite_form = InviteForm.new(params[:invite_form])
    if @invite_form.valid?
      service = InvitationsService.new(@invite_form.email)
      if service.invite_user
        f(:success)
      else
        f(:error, messages: service.errors.full_messages)
      end
    else
      f(:error, messages: @invite_form.errors.full_messages)
    end

    redirect_to root_path
  end

  def accept
    @invite = Invite.not_accepted.find_by!(token: params[:token])
    session[:invite_id] = @invite.id
    redirect_to new_user_path
  end
end
