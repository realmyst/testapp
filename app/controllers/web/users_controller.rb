class Web::UsersController < ApplicationController

  def new
    @signup_form = SignupForm.new
    @signup_form.email = current_invite.email if current_invite
  end

  def create
    @signup_form = SignupForm.new(params[:user])
    if @signup_form.save
      UserRegistrationService.new(@signup_form, current_invite).register
      f(:notice)
      redirect_to new_user_path
    else
      render :new
    end
  end

  def confirm
    @user = User.find_by!(id: params[:id], confirmation_token: params[:confirmation_token])
    if UserRegistrationService.new(@user).confirm
      sign_in(@user)
    else
      f(:error)
    end
    redirect_to root_path
  end

  private
  def current_invite
    @current_invite ||= Invite.not_accepted.find_by(id: session[:invite_id])
  end

end
