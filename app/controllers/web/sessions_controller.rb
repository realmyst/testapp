class Web::SessionsController < ApplicationController
  def new
    @signin_form = SigninForm.new
    if signed_in?
      redirect_to root_path
    else
      render :new
    end
  end

  def create
    @signin_form = SigninForm.new(params[:signin_form])

    if @signin_form.valid?
      sign_in(@signin_form.user)
      redirect_to root_path
    else
      render :new
    end

  end

  def destroy
    sign_out
    redirect_to root_path
  end
end
