class SigninForm
  include BaseFormWithoutActiveRecord

  attribute :email, String
  attribute :password, String

  validates :email, presence: true, email: true
  validates :password, presence: true

  validate :check_authenticate, if: :email
  validate :check_active, if: :check_authenticate

  def user
    @user ||= ::User.find_by(email: email.mb_chars.downcase)
  end

  private

  def check_authenticate
    if !user.try(:authenticate, password)
      errors.add(:password, :user_or_password_invalid)
      false
    else
      true
    end
  end

  def check_active
    if !user.active?
      errors.add(:email, :not_active)
      false
    else
      true
    end
  end

end
