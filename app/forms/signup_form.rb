class SignupForm < User
  include BaseForm
  include ActiveModel::SecurePassword

  validates :password, presence: true, confirmation: true

  permit :email, :password, :password_confirmation
end
