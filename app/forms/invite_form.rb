class InviteForm
  include BaseFormWithoutActiveRecord

  attribute :email, String

  validates :email, presence: true, email: true

end
