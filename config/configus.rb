Configus.build Rails.env do
  env :production do
    host "localhost:3000"

    mailers do
      from "noreply@example.com"
    end

    invites do
      period 1.day
    end
  end

  env :staging, parent: :production do
  end

  env :development, parent: :staging do
    invites do
      period 1.minute
    end
  end

  env :test, parent: :development do
  end
end
