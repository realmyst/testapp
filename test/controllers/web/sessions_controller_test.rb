require 'test_helper'

class Web::SessionsControllerTest < ActionController::TestCase
  def setup
    @user = create(:user, password: 'password')
  end

  test "correct show sign_in page" do
    get :new
    assert_response :ok
  end

  test "redirect if already logged in" do
    sign_in(@user)
    get :new
    assert_response :redirect
  end

  test "sign in if enter correct password/email" do
    attrs = {
      email: @user.email,
      password: "password"
    }

    post :create, signin_form: attrs

    assert_response :redirect
    assert signed_in?
  end

  test "destroy session if sign out" do
    sign_in(@user)
    delete :destroy

    assert_response :redirect
    assert !signed_in?
  end
end
