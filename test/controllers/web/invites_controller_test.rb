require 'test_helper'

class Web::InvitesControllerTest < ActionController::TestCase
  def setup
    @user = create(:user)
    sign_in(@user)
  end

  test "should create invite" do
    attrs = attributes_for :invite
    assert_difference 'Invite.count' do
      post :create, invite_form: {email: attrs[:email]}
    end
    assert_response :redirect
  end

  test "don't allow frequent invites" do
    invite = create :invite_with_attempts
    assert_no_difference 'Invite::Attempt.count' do
      post :create, invite_form: {email: invite.email}
    end
  end

  test "should accept invite" do
    invite = create :invite
    get :accept, token: invite.token
    assert_response :redirect
    assert_equal invite.id, session[:invite_id]
  end
end
