require 'test_helper'

class Web::UsersControllerTest < ActionController::TestCase
  test "show registration page" do
    get :new
    assert_response :ok
  end

  test "create valid user" do
    attrs = attributes_for(:user)
    post :create, user: attrs

    assert_response :redirect

    user = User.find_by(email: attrs[:email])

    assert user, 'user not found'
    assert user.unconfirmed?, 'user state invalid'
  end

  test "confirmation user" do
    user = create(:new_user)
    get :confirm, id: user.id, token: user.confirmation_token

    user.reload
    assert_response :redirect
    assert user.active?, 'user state invalid'
  end

  test "create user with invite" do
    invite = create(:invite)
    attrs = attributes_for(:user, email: invite.email)
    session[:invite_id] = invite.id

    post :create, user: attrs

    user = User.find_by(email: attrs[:email])
    assert user
    assert_equal user.invite, invite
  end
end
