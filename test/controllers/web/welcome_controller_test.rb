require 'test_helper'

class Web::WelcomeControllerTest < ActionController::TestCase
  def setup
    @user = create(:user)
  end

  test "redirect to login if not logged in " do
    get :index
    assert_response :redirect
  end

  test "show page if logged in" do
    sign_in(@user)

    get :index
    assert_response :ok
  end
end
