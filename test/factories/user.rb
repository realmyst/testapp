FactoryGirl.define do
  factory :user do
    state 'active'
    email
    password

    factory :new_user do
      state 'unconfirmed'
    end
  end
end
