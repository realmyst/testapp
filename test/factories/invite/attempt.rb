FactoryGirl.define do
  factory :invite_attempt, class: 'Invite::Attempt' do
    invite
  end
end
