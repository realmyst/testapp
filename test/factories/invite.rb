FactoryGirl.define do
  factory :invite do
    state 'not_accepted'
    email
    token

    factory :accepted_invite do
      state 'accepted'
    end

    transient do
      attempts_count 3
    end

    factory :invite_with_attempts do
      after(:create) do |invite, evaluator|
        create_list(:invite_attempt, evaluator.attempts_count, invite: invite)
      end
    end
  end
end
